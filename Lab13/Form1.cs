﻿using Gidiowania;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;

namespace Lab13
{
    public partial class Form1 : Form
    {
      //  private XmlDocument dok;
        private Gildia gil = new Gildia();
        private Wojwonik w = new Wojwonik();
        XDocument dok;
        string last = "";
        public Form1()
        {
            InitializeComponent();
     
              definicjaXml();
           // wczytaj();
        }

        public void definicjaXml()
        {
            comboBox1.DataSource = gil.foriczuj();
            comboBox1.DisplayMember = "Imie";
            wojwonikBindingSource.DataSource = gil.foriczuj();
            comboBox2.DataSource = new List<string> { "Meska", "Damska" };
            comboBox3.DataSource = typeof(Wojwonik).GetProperties().Select(p => p.Name).ToList();

            dok = new XDocument(new XElement("Gildia"));
            dok.Changed += new EventHandler<XObjectChangeEventArgs>(zmiana);

            foreach(Wojwonik w in gil.foriczuj())
            {
                dok.Root.Add(new XElement("Wojownik",
                    new XAttribute("Płeć",w.Plec),
                    new XElement("Imie", w.Imie),
                    new XElement("Siła",w.Sila),
                    new XElement("Zdrowie",w.Zdrowie),
                    new XElement("Zdobycz",w.NajwiekszaZdobycz)
                    ));
                
            }

           // dok.Save("C:/Users/pacio/Desktop/lolo.xml");
        }
        public void zmiana(object sender, XObjectChangeEventArgs argsy)
        {
            Console.WriteLine($"last: {last} argsy: {argsy.ObjectChange.ToString()}");
          //  if (last == "" || argsy.ObjectChange.ToString() != last)
            {
                Console.WriteLine($"Wykonana operacja na dokumencie: {argsy.ObjectChange.ToString()}");
                XElement elem = sender as XElement;
                last = argsy.ObjectChange.ToString();
                dok.Changed -= zmiana;
                dok.Root.Add(new XElement("Operacja",
                  new XElement(argsy.ObjectChange.ToString(),DateTime.Now.ToString())));
                dok.Changed += zmiana;
                // dok.Root.Add(new XElement("test", "srest"));
                dok.Save("C:/Users/pacio/Desktop/lolo.xml");
                
            }
        }
   

        private void wczytajToolStripMenuItem_Click(object sender, EventArgs e)
        {

            wczytaj();
        }
        public void wczytaj()
        {
            XDocument xel = XDocument.Load("C:/Users/pacio/Desktop/lolo.xml");

            var imie = from i in xel.Descendants("Imie")
                       select i.Value;

            var zdrowie = from s in xel.Descendants("Zdrowie")
                          select s.Value;

            var sila = from s in xel.Descendants("Siła")
                       select s.Value;

            var zdobycz = from z in xel.Descendants("Zdobycz")
                          select z.Value;

            var woj = from z in xel.Descendants("Wojownik")
                      select z.Attribute("Płeć").Value;
            List<Wojwonik> lista = new List<Wojwonik>();
            int licznik = 0;
            foreach (var item in imie)
            {
                Wojwonik w = new Wojwonik(
                       item,
                       Int32.Parse(sila.ElementAt(licznik)),
                       Int32.Parse(zdrowie.ElementAt(licznik)),
                       zdobycz.ElementAt(licznik),
                       woj.ElementAt(licznik)
                    );
                lista.Add(w);
                licznik++;
                Console.WriteLine($"Dodaje wojowniaka {w.Imie} licznik: {licznik}");
            }
            gil.nowaLista(lista);
            wojwonikBindingSource.DataSource = null;
            wojwonikBindingSource.DataSource = lista;
            comboBox1.DataSource = gil.foriczuj();
            comboBox1.DisplayMember = "Imie";
            wojwonikBindingSource.DataSource = gil.foriczuj();
            comboBox2.DataSource = new List<string> { "Meska", "Damska" };
            comboBox3.DataSource = typeof(Wojwonik).GetProperties().Select(p => p.Name).ToList();

        }
        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string obiekt = comboBox1.Text;
            Wojwonik woj = gil.foriczuj().SingleOrDefault(w => w.Imie == obiekt);
            if (woj != null)
            {
                textBox1.Text = woj.Imie;
                textBox2.Text = woj.Sila.ToString();
                textBox3.Text = woj.Zdrowie.ToString();
                textBox4.Text = woj.NajwiekszaZdobycz;
                comboBox2.Text = woj.Plec;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!gil.foriczuj().Any(wo => wo.Imie == textBox1.Text))
            {
                Wojwonik w = new Wojwonik
                {
                    Imie = textBox1.Text,
                    Sila = Int32.Parse(textBox2.Text),
                    Zdrowie = Int32.Parse(textBox3.Text),
                    NajwiekszaZdobycz = textBox4.Text,
                    Plec = comboBox2.Text

                };
                gil.dodajWoja(w);
                ((CurrencyManager)BindingContext[gil.foriczuj()]).Refresh();
                Refresho();
            }
        }
        public void Refresho()
        {
            wojwonikBindingSource.DataSource = null;
            wojwonikBindingSource.DataSource = gil.foriczuj();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string imie = comboBox1.Text;
            Wojwonik woj=gil.foriczuj().Single(w => w.Imie == imie);
            int index = gil.foriczuj().FindIndex(w => w.Imie == imie);
            gil.foriczuj().Remove(woj);
            Wojwonik nowy = new Wojwonik
            {
                Imie = textBox1.Text,
                Sila = Int32.Parse(textBox2.Text),
                Zdrowie = Int32.Parse(textBox3.Text),
                NajwiekszaZdobycz = textBox4.Text,
                Plec = comboBox2.Text
            };
            gil.foriczuj().Insert(index, nowy);
            Refresho();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string imie = comboBox1.Text;
            Wojwonik woj = gil.foriczuj().Single(w => w.Imie == imie);
            gil.foriczuj().Remove(woj);
            Refresho();
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            string obiekt = comboBox3.Text;
            // gil.foriczuj().Sort( s=> s. typeof(Wojwonik).GetProperty(obiekt).GetValue(gil.foriczuj().Single(w => w.GetType().Name == obiekt)));
            // gil.foriczuj().Sort(typeof(Wojwonik).GetProperty(obiekt).GetValue(gil.foriczuj().Select(s=> s.GetType().Name == obiekt).First(f=>f.GetType().Name==obiekt)));
            gil.foriczuj().Sort((x, y) =>
            {
                //Console.WriteLine(x.GetType().Name);
              //  Console.WriteLine(x.GetType().GetProperty(obiekt).PropertyType.Name);
               // Console.WriteLine(x.GetType().GetProperty(obiekt).GetValue(x));
                if (x.GetType().GetProperty(obiekt).PropertyType.Name == "String")
                {
                    string xo = (string)x.GetType().GetProperty(obiekt).GetValue(x);
                    string yo=(string)y.GetType().GetProperty(obiekt).GetValue(y);
                    return xo.CompareTo(yo);

                }
                else
                {
                    Console.WriteLine("zaszedl else");
                    int xo = (int)x.GetType().GetProperty(obiekt).GetValue(x);
                    int yo = (int)y.GetType().GetProperty(obiekt).GetValue(y);
                    Console.WriteLine($"Porownanie xo: {xo} vs yo: {yo}");
                   if (xo > yo)
                    {
                        Console.WriteLine("Argument jeden wiekszy");
                        return -1;
                        
                    }
                    if (xo < yo)
                    {
                        Console.WriteLine("Argument jeden mniejszy");
                        return 1;
                      
                    }
                    else
                    {
                        Console.WriteLine("Argumenty rowne");
                        return 0;
                        
                    }
                   
                }
               
               
            });
            if(radioButton2.Checked == true)
            {
                gil.foriczuj().Reverse();
            }
            Refresho();
            Console.WriteLine("Po refreshu");
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            XDocument xel = XDocument.Load("C:/Users/pacio/Desktop/lolo.xml");

            var imie = from i in xel.Descendants("Imie")
                       select i.Value;
            string agregat = "";
            foreach (var item in imie)
            {
                agregat += $" {item} \n";
            }
            MessageBox.Show(agregat);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            int sila = Int32.Parse(textBox2.Text);
            string plec = comboBox2.Text;
            XDocument xel = XDocument.Load("C:/Users/pacio/Desktop/lolo.xml");

            // var imie = from i in xel.Descendants("Imie")
            //           select i.Value;
            var imie = xel.Descendants("Wojownik")
                   .Where(k => Int32.Parse(k.Element("Siła").Value) > sila && k.Attribute("Pleć").Value == plec)
                   .Select(k => k.Element("Imie").Value);
            StringBuilder agregat = new StringBuilder();
            foreach (string item in imie)
            {
                agregat.Append($" {item} \n");
            }
            MessageBox.Show(agregat.ToString());
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            int zdrowie = Int32.Parse(textBox3.Text);
            XDocument xel = XDocument.Load("C:/Users/pacio/Desktop/lolo.xml");
            List<XElement> elementy = xel.Descendants("Wojownik")
                     .Where(k => Int32.Parse(k.Element("Zdrowie").Value) < zdrowie).ToList();
                     

            foreach(XElement x in elementy)
            {
                Console.WriteLine($"Usuwam {x.Element("Imie").Value}");
                x.Remove();
            }

            zapisz(xel);
            wczytaj();

        }
        public void zapisz(XDocument elementy)
        {

            // dok = new XmlDocument();
            /*  StringBuilder agregar = new StringBuilder();
              foreach (XElement item in elementy)
              {
                  agregar.Append(item.ToString());
              }
              */
            // dok.LoadXml(agregar.ToString());
            // dok.Save("C:/Users/pacio/Desktop/lolo.xml");
            elementy.Save("C:/Users/pacio/Desktop/lolo.xml");
            
        }

        private void button7_Click(object sender, EventArgs e)
        {
            string wybrany = comboBox1.Text;

            XDocument xel = XDocument.Load("C:/Users/pacio/Desktop/lolo.xml");
            
            List<XElement> elementy = xel.Descendants("Wojownik")
                   .Where(k => k.Element("Imie").Value == wybrany).ToList();
          
            string imie = textBox1.Text;
            string sila = textBox2.Text;
            string zdrowie = textBox3.Text;
            string zdobycz = textBox4.Text;
            string plec=comboBox2.Text;

            XElement woj=new XElement("Wojownik",
                    new XAttribute("Płeć", plec),
                    new XElement("Imie", imie),
                    new XElement("Siła", sila),
                    new XElement("Zdrowie", zdrowie),
                    new XElement("Zdobycz", zdobycz)
                    );

            xel.Descendants("Wojownik")
                    .Single(k => k.Element("Imie").Value == wybrany).AddAfterSelf(woj);
              //  List<XElement> list= xel.Descendants("Wojownik").ToList();

           // list.Single(k => k.Element("Imie").Value == wybrany).AddAfterSelf(woj);
            //xel = new XDocument();
         //   XElement wojownik = new XElement("wojownik");
         //   foreach (XElement ele in list)
            {
          //      wojownik.Add(ele);
           }
          //  xel.Add(woj);

            zapisz(xel);
            Refresho();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gidiowania
{
    public class Wojwonik 
    {
        public Wojwonik() 
        {

        }
        public Wojwonik(string Imie, int Sila, int Zdrowie, string NajwiekszaZdobycz, string Plec)
        {
            this.Imie = Imie;
            this.Sila = Sila;
            this.Zdrowie = Zdrowie;
            this.NajwiekszaZdobycz = NajwiekszaZdobycz;
            this.Plec = Plec;
        }
        public string Imie { get; set; }
        public int Sila { get; set; }
        public int Zdrowie { get; set; }
        public string NajwiekszaZdobycz { get; set; }
        public string Plec { get; set; }

   
    }
}

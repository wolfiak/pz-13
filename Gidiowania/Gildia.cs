﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gidiowania
{
    public class Gildia
    {
       private List<Wojwonik> lista = new List<Wojwonik>() {
           new Wojwonik("Riczardz",100,100,"DonKarczow","Meska"),
           new Wojwonik("Kazimierz",80,80,"Szczur","Damska"),
           new Wojwonik("Testomierz",110,110,"Kazimierz","Meska")
       };
        
        public Wojwonik wezWojownika(int imie)
        {
            return lista.SingleOrDefault(w=> w.Imie.Equals(imie));
        }
        public void dodajWoja(Wojwonik w)
        {
            lista.Add(w);
        }
        public List<Wojwonik> foriczuj()
        {
            return lista;
        }
        public void nowaLista(List<Wojwonik> lista)
        {
            this.lista = lista;
        }
    }
}
